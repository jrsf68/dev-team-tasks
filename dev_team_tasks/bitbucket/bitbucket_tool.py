"""Bitbucket tool procedures implementation"""

import re
from datetime import datetime, timedelta
from uuid import uuid4

from loguru import logger
from requests import get


async def get_commits_from_bitbucket(
    management_tool: dict, repositories: list, developers: list
):
    """Extract commits from all bitbucket repositories"""
    all_tasks = []
    for repository in repositories:
        bitbucket_tasks = await get_commits_from_bitbucket_repository(
            management_tool, repository, developers
        )
        if bitbucket_tasks is None:
            return bitbucket_tasks
        if bitbucket_tasks == []:
            continue
        all_tasks.extend(bitbucket_tasks)
    return all_tasks


async def get_commits_from_bitbucket_repository(
    management_tool: dict, repository: dict, developers: list
):
    url = f'{management_tool["data_url"]}/{repository["name"]}/commits'

    try:
        response = get(
            url,
            auth=(management_tool['username'], management_tool['password']),
        )
        commits = response.json()['values']
        list_commits = []
        for commit in commits:
            if 'user' not in commit['author']:
                continue
            dt = (
                datetime.fromisoformat(commit['date']) - timedelta(hours=3)
            ).strftime('%Y-%m-%d %H:%M:%S')
            if (
                commit['type'] == 'commit'
                and commit['message'][:14] != "Merge branch '"
                and [
                    developer['bitbucket_uuid']
                    for developer in developers
                    if developer['bitbucket_uuid']
                    == commit['author']['user']['uuid']
                ]
                and dt > repository['last_read_date']
                and dt < datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            ):
                commit_read = dict()
                commit_read['uuid'] = str(uuid4())
                commit_read['created'] = datetime.now().strftime(
                    '%Y-%m-%d %H:%M:%S'
                )
                commit_read['tool'] = 'bitbucket'
                commit_read['developer_full_name'] = [
                    developer['full_name']
                    for developer in developers
                    if developer['bitbucket_uuid']
                    == commit['author']['user']['uuid']
                ][0]
                commit_read['developer_id'] = commit['author']['user']['uuid']
                commit_read['project'] = commit['repository'][
                    'full_name'
                ].rsplit('/', 1)[-1]
                commit_read['task_id'] = commit['hash']
                commit_read['task'] = (commit['message']).replace('\n', '')
                commit_read['completion_date'] = dt
                commit_read['status'] = 'Complited'
                list_commits.append(commit_read)
        return list_commits
    except Exception as e:
        logger.debug(f'Error reanding from {repository["name"]} ({e})')
        return None
