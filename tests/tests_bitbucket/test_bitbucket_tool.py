"""Test bitbucket procedures
"""

import pytest

from dev_team_tasks.bitbucket.bitbucket_tool import get_commits_from_bitbucket


@pytest.mark.asyncio
async def test_get_commits_from_bitbucket():
    management_tool = {
        'tool': 'bitbucket',
        'username': 'fake',
        'password': 'fake',
        'data_url': 'http://0.0.0.0:8001',
    }
    repositories = [
        {
            'tool': 'bitbucket',
            'name': 'sigplan',
            'last_read_date': '2023-10-01 00:00:00',
        }
    ]
    developers = [
        {
            'full_name': 'LEILANE MONTEIRO DA CRUZ',
            'bitbucket_uuid': '{ea8bda0f-9fc4-4ed6-a3b1-8a3b90f2b8bf}',
        }
    ]

    commits = await get_commits_from_bitbucket(
        management_tool=management_tool,
        repositories=repositories,
        developers=developers,
    )

    assert commits is not None
