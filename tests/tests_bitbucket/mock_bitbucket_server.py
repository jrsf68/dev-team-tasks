from json import load

from fastapi import FastAPI

app = FastAPI()


@app.get('/sigplan/commits')
def mock_gitlab_server():
    with open('mock_bitbucket_commit.json', 'r') as json_file:
        data = load(json_file)
    print(f'data: {data}')
    return data
